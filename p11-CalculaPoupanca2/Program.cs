﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace p11_CalculaPoupanca2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Executando projeto 11");

            double valorInvestido = 15000.0;

            for (int i = 1; i <= 12; i++)
            {
                valorInvestido *= 1.0036;
                Console.WriteLine("Após " + i + " meses, você terá R$" + valorInvestido);
            }

            Console.WriteLine("Programa finalizado, pressione enter para sair. . .");
            Console.ReadLine();
        }
    }
}
