﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9_escopo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Executando o projeto 9");

            int idadeJoao = 16;
            //int quantidadePessoas = 2;
            //bool acompanhado = quantidadePessoas > 1;

            bool acompanhado = true;

            string mensagemAdicional = "";

            if (acompanhado)
            {
                mensagemAdicional = "João está acompanhado!";
            }

            if (idadeJoao >= 18 || acompanhado)
            {
                Console.WriteLine("João possui mais de 18 anos de idade ou está acompanhado. Pode entrar.");
                Console.WriteLine(mensagemAdicional);
            }
            else
            {
                Console.WriteLine("João não possui mais de 18 anos. Não pode entrar.");
            }

            Console.WriteLine("Programa finalizado. Pressione enter para fechar. . .");
            Console.ReadLine();

        }
    }
}
