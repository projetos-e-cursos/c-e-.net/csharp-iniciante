﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_ConversoesEOutrosTiposNumericos
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Executando o projeto 4");

            double salario = 1200.50;

            int salarioEmInteiro; // int suport numeros inteiros e é de 32 bits;
            salarioEmInteiro = (int)salario;
            Console.WriteLine(salarioEmInteiro);

            long idade = 13000000000000; // long suporta números inteiros e é de 64 bits;
            Console.WriteLine(idade);

            short quantidadeProdutos; // short suporta numeros inteiros e é de 16 bits;
            quantidadeProdutos = 150;
            Console.WriteLine(quantidadeProdutos);

            float altura = 1.72f;
            Console.WriteLine(altura);

            Console.WriteLine("Acabou a execução, pressione Enter para encerrar. . .");
            Console.ReadLine();
        }
    }
}
