﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5_CaracteresETextos
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Executando projeto 5");

            char primeiraLetra = 'a';
            Console.WriteLine("Primeira letra = " + primeiraLetra);

            primeiraLetra = (char)65;
            Console.WriteLine(primeiraLetra);

            primeiraLetra = (char)(primeiraLetra + 1);

            string titulo = "Alura Cursos de tecnologia " + 2020;
            Console.WriteLine(titulo);

            string vazia = "";
            vazia = "abc";
            Console.WriteLine(vazia);

            string cursosProgramacao = @" - .NET - Java - JavaScript"; 
            Console.WriteLine("Programa finalizado. Pressione enter para fechar. . .");
            Console.ReadLine();
        }
    }
}
