﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P10_CalculaPoupanca
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Executando projeto 10");

            double valorInvestido = 1000.0;

            // 0.36% == 0.0036
            int count = 1;
            while (count <=12)
            {
                valorInvestido += valorInvestido * 0.0036;
                Console.WriteLine("Após " + count + " meses, você terá R$" + valorInvestido);
                count++;
            }

            Console.WriteLine("Programa finalizado, pressione enter para sair. . .");
            Console.ReadLine();
        }
    }
}
